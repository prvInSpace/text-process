"""
This module contain very simple function to help normalise utf8 text.
More specifically, normalising characters that have more than one utf8
version or duplication of characters.
"""

import re

MULTIPLE_SPACES_AND_TABS = re.compile(r"[ \t]{2,}")
SINGLE_QUOTATION_MARK = re.compile("[\u02BB\u02BC\u066C\u2018-\u201B\u275B\u275C\u2039\u203A]")
DOUBLE_QUOTATION_MARK = re.compile("[\u201C-\u201F\u2033\u275D\u275E\u301D\u301E\u00AB\u00BB]")
APOSTROPHE = re.compile(
    "[\u0027\u02B9\u02BB\u02BC\u02BE\u02C8\u02EE\u0301\u0313\u0315\u055A\u05F3\u07F4\u07F5\u1FBF\u2018\u2019\u2032\uA78C\uFF07]"
)

HTML_TAGS = re.compile("</?.*?>")


def cleanup_utf8_chars(s: str) -> str:
    """Normalises single and double quotation marks and apostrophes to the standard ones"""
    s = SINGLE_QUOTATION_MARK.sub("'", s)
    s = DOUBLE_QUOTATION_MARK.sub('"', s)
    return APOSTROPHE.sub("'", s)


def cleanup_spaces(s: str) -> str:
    """Normalises any space or tabs to a single space"""
    return MULTIPLE_SPACES_AND_TABS.sub(" ", s)


def remove_xml_tags(s: str, replacement: str = " ") -> str:
    """Very simple function that removes very simple XML tags"""
    return HTML_TAGS.sub(replacement, s)
